Игра Сапер.

#### Стек:
* React, Typescript

## Инструкция по запуску

##### 1. Скачать проект: `git clone https://github.com/vladislavserov/mine-sweeper.git`

##### 2. Установить зависимости: `yarn`

##### 3. Запустить сервер: `yarn start`

