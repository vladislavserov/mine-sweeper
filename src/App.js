import logo from './logo.svg';
import './App.css';
import Main from './components/Main/Main.tsx';

function App() {
  return <Main style={{display: 'flex',  flexDirection: 'row', flexWrap: 'wrap'}}/>;
}

export default App;
