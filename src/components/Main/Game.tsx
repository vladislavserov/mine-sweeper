import { useEffect, useMemo } from 'react';
import { GameArea, GameAreaConfig, Square, GameContainer, GameStatus  } from '.'
import { getAroundSquaresIndexes, coordsToIndex } from './utils'

const openSquareColor = '#8c8c8c'
const hiddenSquareColor = '#c0c0c0'
const MINE_SYMBOL = 'x'
const NO_MINES_NEARBY_SYMBOL = ''
const FLAG_SYMBOL = String.fromCharCode(9873)

const buttonLabel = (item: GameArea) => {
  if (item.isMine) {
    return MINE_SYMBOL
  }
  return item.mineAround === 0 ? NO_MINES_NEARBY_SYMBOL : `${item.mineAround}`    
}

interface GameProps {
  config: GameAreaConfig,
  gameArea: GameArea[],
  setStep: (step: GameStatus) => void,
  setGameArea: (gameArea: GameArea[]) => void,
}

const Game = (props: GameProps) => {
  const { config, gameArea, setStep, setGameArea } = props;

  const markedSquares = useMemo(() => {
    return gameArea.filter((square) => square.marked).length 
  }
  , [gameArea]);

  useEffect(() => {
    if (config) {
      const notClickedSquaresQuantity = gameArea.filter((item) => !item.clicked).length
      const onlyMinesLeft = notClickedSquaresQuantity === config.mine
      const notMarkedMines = gameArea.filter((square) => square.isMine).filter((square) => square.marked).length
      if (onlyMinesLeft && (notMarkedMines === 0)) {
        setStep(GameStatus.playerWon)
      }
    }
  }
  , [gameArea, config])

  const buttonClickHandler = (item: GameArea) => {
    const i = coordsToIndex(item.x, item.y, config)
    if (!item.clicked) {
      if (item.isMine) {
          setStep(GameStatus.playerLost)
      } else {
        if (mineAroundCounter(i) > 0) {
          const newGameArea = [...gameArea]
          newGameArea[i] = {...gameArea[i], label: buttonLabel(gameArea[i]), clicked: true}
          setGameArea(newGameArea)
        } else {
          const newGameArea = showEmptySquares(gameArea, i)
          setGameArea(newGameArea)
        }
      }
    }
  }

  const showEmptySquares = (gameAria: GameArea[], index: number) => {
    const newGameArea = [...gameArea]
    openConnectedEmptySquares(newGameArea, index)
    return newGameArea
  }

  const openConnectedEmptySquares = (gameArea: GameArea[], index: number) => {
    const currentSquare = gameArea[index];
    if (!currentSquare.clicked && !currentSquare.isMine && currentSquare.mineAround === 0) {
      gameArea[index] = {...gameArea[index], label: buttonLabel(gameArea[index]), clicked: true}
      const nearbySquareIndexes = getAroundSquaresIndexes(gameArea, index, config)
      const nearbyEmptySquareIndexes = nearbySquareIndexes.filter((index) => !gameArea[index].isMine)
      nearbyEmptySquareIndexes.forEach((index) => openConnectedEmptySquares(gameArea, index)) 
    }
    if (!currentSquare.clicked && !currentSquare.isMine) {
      gameArea[index] = {...gameArea[index], label: buttonLabel(gameArea[index]), clicked: true}
    }
  }

  const mineAroundCounter = (i: number) => {
    return getAroundSquaresIndexes(gameArea, i, config)
           .filter((index : number) => gameArea[index].isMine)
           .length
  }

  const onContextMenuClickHandler = (item: GameArea, event: React.MouseEvent) => {
    event.preventDefault();
    if (item.marked) {
      const newGameArea = [...gameArea]
      const index = coordsToIndex(item.x, item.y, config)
      newGameArea[index] = {...newGameArea[index], label: NO_MINES_NEARBY_SYMBOL, marked: false}
      setGameArea(newGameArea)
    } else {
      const newGameArea = [...gameArea]
      const index = coordsToIndex(item.x, item.y, config)
      newGameArea[index] = {...newGameArea[index], label: FLAG_SYMBOL, marked: true}
      setGameArea(newGameArea)
    }
    return false
  }

  const handleSquareClick = ( item: GameArea) => {
    buttonClickHandler(item)
  }

  const renderInnerGameArea = (gameArea: GameArea[]) => {
    return gameArea.map((item) => {
      const color = item.clicked ? openSquareColor : hiddenSquareColor 
      return <Square onClick={() => handleSquareClick(item)} color={color} 
        onContextMenu={(event) => onContextMenuClickHandler(item, event)}>{item.label}</Square>
    })
  }
  
  return (
    <div style={{display: 'flex', flexDirection: 'column', flexWrap: 'wrap'}} >
      <h2>Помечено: {markedSquares}</h2>
      <br/>
      <h2>Мин всего: {config.mine}</h2>
      <GameContainer width={`${config.width*20}px`}>
        {renderInnerGameArea(gameArea)}
      </GameContainer>
    </div>
  )
}

export default Game