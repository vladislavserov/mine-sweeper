import styled from '@emotion/styled'

interface GameContainerProps {
  width: string,
  children?: JSX.Element[] 
} 

const GameContainer = styled.div<GameContainerProps>(props => (
  {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: `${props.width}`,
    height: 'auto',
  }
))

export default GameContainer;

