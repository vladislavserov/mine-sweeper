import { useEffect, useState } from 'react'
import { StartDisplay } from '../StartDisplay'
import { GameArea, GameAreaConfig, Game, GameStatus } from '.'
import { INITIAL_CONFIG } from '../../shared/consts'
import { getRandomNumber, getGameArea, setMineAroundQuantity } from './utils'

const Main = (): JSX.Element => {
  const [ step, setStep ] = useState(GameStatus.notStarted)
  const [ gameArea, setGameArea ] = useState<GameArea[]>([])
  const [ config, setConfig ] = useState<GameAreaConfig>(INITIAL_CONFIG)
  const [ hasGameStarted, setHasGameStarted ] = useState(false)

  useEffect(() => {
    const miningGameArea = (newGameArea: GameArea[]) => {
      if (config) {
        const mineIndexes = Array.from(new Array(config.mine)).map((_el) => getRandomNumber(-1, config.width*config.height))
        mineIndexes.map((index) => newGameArea[index].isMine = true)
        return newGameArea
      }
      return newGameArea
    }
    if (hasGameStarted) {
      const newGameArea = getGameArea(config)
      setGameArea(setMineAroundQuantity(miningGameArea(newGameArea), config))
      setStep(GameStatus.started)
    } 
  }, [hasGameStarted, config])

  const startButtonClickHandler = () => {
    setHasGameStarted(false)
    setHasGameStarted(true)
    setStep(GameStatus.notStarted)
  }

  switch(step) {
    case GameStatus.started: 
      return <Game config={config}
                   gameArea={gameArea} 
                   setStep={setStep}
                   setGameArea={setGameArea}
            />
    case GameStatus.playerWon: 
      return <><p>Ты победил!</p><button onClick={startButtonClickHandler}>Рестарт</button></>
    case GameStatus.playerLost: 
      return <><p>Ты проиграл!</p><button onClick={startButtonClickHandler}>Рестарт</button></>
    case GameStatus.notStarted:  
    default:
      return <div><StartDisplay setConfig={setConfig} setGameIsStarted={setHasGameStarted}/></div>
  } 
}

export default Main