import styled from '@emotion/styled'

const Square = styled.div(props => (
  {
    display: 'flex',
    height: '20px',
    width: '20px',
    backgroundColor: `${props.color}`,
    border: '1px solid black',
    borderRadius: '3%',
    justifyContent: 'center',
  }
))

export default Square;