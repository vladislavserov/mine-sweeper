import Square from './Square'
import GameContainer from './GameContainer'
import Main from './Main'
import Game from './Game'

export enum GameStatus {
  notStarted,
  started,
  playerWon,
  playerLost
}

export interface GameArea {
  mineAround: number,
  isMine: boolean,
  label: string,
  x: number,
  y: number,
  clicked: boolean,
  marked: boolean
}

export interface GameAreaConfig {
  width: number,
  height: number,
  mine: number
}

export { Square, GameContainer, Main, Game };