import { GameAreaConfig, GameArea } from '..'

export const getAroundSquaresIndexes = (field: GameArea[], i: number, config: GameAreaConfig) : number[] => {
  const x = field[i].x
  const y = field[i].y
  const maxY = config.height - 1
  const maxX = config.width - 1

  const xCoords = [x-1, x, x+1].filter(num => num >=0 && num <= maxX)
  const yCoords = [y-1, y, y+1].filter(num => num >=0 && num <= maxY)

  return xCoords
         .flatMap((x) => yCoords.map((y) => coordsToIndex(x, y, config)))
         .filter(el => el !== i)
}

export const coordsToIndex = (x: number, y: number, config: GameAreaConfig): number => {
  return x*config.height+y
}