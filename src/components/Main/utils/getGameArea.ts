import { GameArea, GameAreaConfig } from '..'

export const getGameArea = (config : GameAreaConfig) : GameArea[] => {
  const height = Array.from(new Array(config.height))
  const width = Array.from(new Array(config.width))
  return width.flatMap((_ ,x) => 
    height.map((_, y) => {
      return {
        mineAround: 0,
        isMine: false,
        label: '',
        x: x,
        y: y,
        clicked: false,
        marked: false
      }
    }
  ))
}