export const getRandomNumber = (min: number, max: number) => Math.trunc(Math.random() * (max - min) + min)
