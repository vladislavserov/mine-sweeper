import { getRandomNumber } from './getRandomNumber'
import { getGameArea } from './getGameArea'
import { setMineAroundQuantity } from './setMineAroundQuantity'
import { getAroundSquaresIndexes, coordsToIndex } from './getAroundSquaresIndexes'

export { getRandomNumber, getGameArea, setMineAroundQuantity, getAroundSquaresIndexes, coordsToIndex }