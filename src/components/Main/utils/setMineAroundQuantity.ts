import { GameArea, GameAreaConfig } from '..'
import { getAroundSquaresIndexes } from './getAroundSquaresIndexes'

export const setMineAroundQuantity = (field: GameArea[], config: GameAreaConfig) => {
  return field.map((item, i) => {
    if (!item.isMine) {
      const mineAround = getAroundSquaresIndexes(field, i, config)
        .filter((index : number) => field[index].isMine).length
      return {...item, mineAround}
    }
    return item
  })
}