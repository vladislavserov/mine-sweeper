import { Form, Input, Button } from 'antd';
import { GameAreaConfig } from './Main';

const layout = {
  labelCol: { span: 9 },
  wrapperCol: { span: 9 },
};
const tailLayout = {
  wrapperCol: { offset: 10, span: 16 },
};

interface StartDisplayForm {
  heightWidth: number,
  mine: number
}

interface StartDisplayProps {
  setConfig: (config: GameAreaConfig) => void, 
  setGameIsStarted: (gameIsStarted: boolean) => void
}

export const StartDisplay = ({ setConfig, setGameIsStarted } : StartDisplayProps) : JSX.Element => {
  const [form] = Form.useForm();
  const onFinish = (values: StartDisplayForm) => {
    if (values.mine < values.heightWidth*values.heightWidth) {
      setConfig({
        width: +values.heightWidth,
        height: +values.heightWidth,
        mine:  +values.mine
      })
      setGameIsStarted(true)
    } else {
      alert('Количество мин должно быть меньше количества клеток!')
      form.resetFields();
    }
  };

  return (
    <Form
      {...layout}
      form={form}
      name="basic"
      initialValues={{ remember: true }}
      onFinish={onFinish}
    >
      <Form.Item
        label="Высота/Ширина"
        name="heightWidth"
        rules={[{ required: true, message: 'Введите высоту!' }]}
        initialValue='10'
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Мины"
        name="mine"
        rules={[{ required: true, message: 'Введите количество мин!' }]}
        initialValue='20'
      >
        <Input />
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
          Старт
        </Button>
      </Form.Item>
    </Form>
  );
};